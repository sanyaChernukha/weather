import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable, throwError, observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Forecast } from '../shared/forecast/forecast.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private weatherUrl = 'https://api.openweathermap.org/data/2.5/forecast?cnt=16&units=metric&';
  private appid = 'e7eb01eab6e030caeb34ba5079f9cdbb';

  constructor(private http: HttpClient) {}

  getWeather(city: string | number) {
    const url = +city
      ? `${this.weatherUrl}zip=${city}&appid=${this.appid}`
      : `${this.weatherUrl}q=${city}&appid=${this.appid}`;
    return this.http.get<Forecast>(url).pipe(catchError(this.handleError));
  }

  /**
   *
   * It processes a request error
   * @param error   http request error for processing
   * @returns throwError
   */
  private handleError(error: HttpErrorResponse) {
    return throwError(`Something bad happened; Code ${error.status}, ` + `body: ${error.error.message}`);
  }
}
