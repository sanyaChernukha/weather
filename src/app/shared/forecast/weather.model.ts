import { WeatherMain } from './weatherMain.model';
import { WearherDescriprion } from './wearherDescriprion.model';

export class Weather {
  clouds: number;
  dt: number;
  dt_txt: string;
  shortDate: string;
  main: WeatherMain;
  weather: WearherDescriprion;
  wind: number;

  constructor(data: any = {}) {
    this.clouds = data.clouds ? data.clouds.all : null;
    this.dt = data.dt || null;
    this.dt_txt = data.dt_txt || null;
    this.shortDate =
      new Date(data.dt_txt).toLocaleDateString('en-US', {
        weekday: 'long',
        month: 'long',
        day: 'numeric'
      }) || null;
    this.main = data.main || null;
    this.weather = data.weather ? data.weather.map(item => new WearherDescriprion(item)) : [];
    this.wind = data.wind ? data.wind.speed : null;
  }
}
