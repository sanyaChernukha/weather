export class City {
  coord: Coord;
  country: string;
  id: number;
  name: string;
  population: number;

  constructor(data: any = {}) {
    this.coord = data.coord || null;
    this.country = data.country || null;
    this.id = data.id || null;
    this.name = data.name || null;
    this.population = data.population || null;
  }
}

interface Coord {
  lat: number;
  lon: number;
}
