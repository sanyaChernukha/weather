import { City } from './city.model';
import { Weather } from './weather.model';

export class Forecast {
  city: City | null;
  cod: string | null;
  list: Weather[];
  message: string | null;

  constructor(data: any = {}) {
    this.city = new City(data.city) || null;
    this.cod = data.cod || null;
    this.list = data.list ? data.list.map(item => new Weather(item)) : [];
    this.message = data.message || null;
  }
}
