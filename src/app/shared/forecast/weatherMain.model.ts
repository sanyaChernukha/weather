export class WeatherMain {
  grnd_level: number;
  humidity: number;
  pressure: number;
  sea_level: number;
  temp: number;
  temp_kf: number;
  temp_max: number;
  temp_min: number;

  constructor(data: any = {}) {
    this.grnd_level = data.grnd_level || null;
    this.humidity = data.humidity || null;
    this.pressure = data.pressure || null;
    this.sea_level = data.sea_level || null;
    this.temp = data.temp || null;
    this.temp_kf = data.temp_kf || null;
    this.temp_max = data.temp_max || null;
    this.temp_min = data.temp_min || null;
  }
}
