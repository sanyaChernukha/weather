export class WearherDescriprion {
  description: string;
  icon: string;
  id: number;
  main: string;

  constructor(data: any = {}) {
    this.description = data.description || null;
    this.icon = data.icon || null;
    this.id = data.id || null;
    this.main = data.main || null;
  }
}
