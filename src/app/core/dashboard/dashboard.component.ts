import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { HttpService } from '../../shared/http.service';
import { Forecast } from '../../shared/forecast/forecast.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [HttpService]
})
export class DashboardComponent implements OnInit {
  private forecastData = new Forecast();
  private customCity = 'Dnipro';
  private loading = false;
  private error = '';

  constructor(private httpService: HttpService, private title: Title) {}

  ngOnInit() {
    this.getWeather(this.customCity);
  }

  private chooseCity() {
    this.error = '';
    this.getWeather(this.customCity);
  }

  private getWeather(city: string) {
    this.loading = true;
    this.httpService.getWeather(city).subscribe(
      (result: any) => {
        this.loading = false;
        this.forecastData = new Forecast(result);
        this.title.setTitle(`Weather in ${this.forecastData.city.name}`);
      },
      (error: any) => {
        this.error = error;
        this.loading = false;
        this.title.setTitle('Weather');
      }
    );
  }
}
