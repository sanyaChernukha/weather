import { Component, OnInit, Input, AfterContentChecked } from '@angular/core';

import { Chart } from 'chart.js';
import { HttpService } from '../../shared/http.service';
import { Weather } from '../../shared/forecast/weather.model';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, AfterContentChecked {
  @Input() weatherArr: Weather[];
  @Input() type: string;
  private chart = { canvas: null };
  private dates = [];
  private params = [];

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    switch (this.type) {
      case 'clouds':
        this.params = this.weatherArr.map(item => item.clouds);
        break;
      case 'wind':
        this.params = this.weatherArr.map(item => item.wind);
        break;
      default:
        this.params = this.weatherArr.map(item => item.main[this.type]);
        break;
    }
    this.dates = this.weatherArr.map(item =>
      new Date(item.dt * 1000).toLocaleDateString('en', { month: 'short', day: 'numeric' })
    );
  }

  ngAfterContentChecked() {
    if (document.getElementById(`${this.type}-chart`) && this.chart.canvas == null) {
      this.chartInit();
    }
  }

  chartInit() {
    this.chart = new Chart(`${this.type}-chart`, {
      type: 'bar',
      data: {
        labels: this.dates,
        datasets: [
          {
            data: this.params,
            borderColor: '#3cba9f',
            fill: false
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              display: true
            }
          ],
          yAxes: [
            {
              display: false
            }
          ]
        }
      }
    });
  }
}
